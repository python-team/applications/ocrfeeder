<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="ocrconfiguration" xml:lang="sv">

<info>
    <link type="guide" xref="index#configuration"/>
    <link type="seealso" xref="automaticrecognition"/>
    <link type="seealso" xref="manualeditionandcorrection"/>
    <desc>Konfigurera OCR-motorerna för att känna igen texten</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Erik Sköldås</mal:name>
      <mal:email>erik.skoldas@tele2.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Konfiguration av OCR-motorer</title>

<p><app>OCRFeeder</app> använder systemomfattande OCR-motorer för att extrahera texten från bilder. Det betyder att en OCR-motor som kan användas från kommandoraden bör också användas i <app>OCRFeeder</app>.</p>

<section id="automatic">

<title>Automatisk identifiering av OCR-motorer</title>

<p>OCR-motorerna (<em>Tesseract</em>, <em>GOCR</em>, <em>Ocrad</em> och <em>Cuneiform</em>) är redan automatiskt identifierade och konfigurerade i de flesta system, första gången <app>OCRFeeder</app> körs.</p>

<p>Om en OCR-motor installeras efter att <app>OCRFeeder</app> redan har konfigurerat en motor, kommer den inte automatiskt att konfigureras men beroende på motorn kan man enkelt gå till dialogrutan för <gui>OCR-motorer</gui> och välja den i listan över upptäckta motorer efter att ha tryckt på <gui>Identifiera</gui>.</p>

<note style="tip"><p>Tidigare konfigurerade OCR-motorer kan bli identifierade på nytt, och det är upp till användaren att avmarkera dessa motorer om de inte ska läggas till igen.</p></note>

</section>

<section id="manual">

<title>Manuell konfiguration</title>

<p>De för närvarande konfigurerade OCR-motorerna visas i dialogrutan <gui>OCR-motorer</gui> som kan öppnas från <guiseq><gui>Verktyg</gui><gui>OCR-motorer</gui></guiseq>.</p>

<p>Förutom att visa de konfigurerade OCR-motorerna, ger dialogrutan för <gui>OCR-motor</gui> möjligheten att lägga till nya motorer, redigera eller ta bort de aktuella samt upptäcka motorer som är installerade i systemet.</p>

<p>När du lägger till eller redigerar en OCR-motor (genom att antingen klicka på knappen <gui>Lägg till</gui> eller <gui>Redigera</gui>), visas en dialog med följande fält:</p>

<list>
    <item><p><gui>Namn</gui>: Motorns namn. Detta namn kommer att användas i hela gränssnittet när det hänvisar till motorn;</p></item>
    <item><p><gui>Bildformat</gui>: Bildformatet som motorn känner igen (till exempel <em>TIF</em> i fallet med <em>Tesseract</em>);</p></item>
    <item><p><gui>Felsträng</gui>: Vissa motorer ersätter okända tecken med ett annat, fördefinierat tecken (till exempel <em>_</em> i fallet med <em>GOCR</em>).</p></item>
    <item><p><gui>Sökväg till motor</gui>: Sökvägen i systemet till motorns körbara fil (till exempel, <em>/usr/bin/tesseract</em>).</p></item>
    <item><p><gui>Argument för motorn</gui>: Argumenten som matar en bild till motorn och gör att den matar ut den igenkända texten till standardutmatningen. <app>OCRFeeder</app> driver motorn med dessa argument som om den befann sig i kommandoraden och letar efter den igenkända texten i standard ut. Vissa motorer gör redan detta, som <em>Ocrad</em> och <em>GOCR</em> medan andra, som <em>Tesseract</em>, skriver texten i en fil.</p>
    <p>Eftersom sökvägen till bilden som ska läsas in alltid behövs, tillhandahålls ett speciellt argument <em>$IMAGE</em> för detta och kommer att ersättas av bildsökvägen när motorn körs. För de fall där ett filnamn behövs, som det som nämnts tidigare, tillhandahålls ett särskilt argument <em>$FILE</em> och kommer att ersättas av ett temporärt filnamn.</p>
    <p>Så, med <em>Tesseract</em> (som skriver den igenkända texten i en fil), skulle argumenten vara <em>$IMAGE $FILE; cat $FILE.txt; rm $FILE</em>.</p></item>

</list>

<note style="advanced"><p>Motorernas konfiguration lagras i en egen XML-fil i användarens hemkatalog under <em>.config/ocrfeeder/engines/</em>.</p></note>

</section>

</page>
