<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="projects" xml:lang="ru">

<info>
    <link type="guide" xref="index#projects"/>
    <desc>Загрузка и сохранение проектов</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  </info>

<title>Проекты</title>

<p>Иногда пользователю может понадобиться сохранить выполненную часть работы и продолжить её позднее. Для таких случаев <app>OCRFeeder</app> поддерживает возможность сохранять и загружать проекты.</p>

<p>Проекты — это сжатые файлы с расширением <em>ocrf</em>, в которых хранится информация о страницах (изображениях) и областях содержимого.</p>

<section id="save">
<title>Сохранение проекта</title>

<p>После выполнения некоторой работы с изображением можно создать проект, выполнив команду меню <guiseq><gui>Файл</gui><gui>Сохранить</gui></guiseq> или <guiseq><gui>Файл</gui><gui>Сохранить как…</gui></guiseq>. Можно также воспользоваться комбинациями клавиш <keyseq><key>Control</key><key>S</key></keyseq> или <keyseq><key>Control</key><key>Shift</key><key>S</key></keyseq>. Откроется диалог сохранения файла, в котором следует указать имя и место размещения проекта.</p>

</section>

<section id="load">
<title>Загрузка проекта</title>

<p>Чтобы открыть сохранённый ранее проект, выполните команду меню <guiseq><gui>Файл</gui><gui>Открыть</gui></guiseq> или нажмите <keyseq><key>Control</key><key>O</key></keyseq>.</p>

</section>

<section id="append">
<title>Добавление проекта</title>

<p>Иногда бывает полезно объединить два или более проектов, чтобы создать один документ со страницами из нескольких проектов <app>OCRFeeder</app>. Для этого нужно просто загрузить страницы из выбранного проекта в текущий проект. Нажмите <guiseq><gui>Файл</gui><gui>Добавить проект</gui></guiseq> и выберите нужный проект.</p>

</section>

<section id="clear">
<title>Очистка проекта</title>

<p>Если нужно удалить всю информацию из проекта (например, чтобы начать его заново), выберите <guiseq><gui>Правка</gui><gui>Очистить проект</gui></guiseq>.</p>

</section>


</page>
