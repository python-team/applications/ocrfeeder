<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="finetuning" xml:lang="gl">

<info>
    <link type="guide" xref="index#configuration"/>
    <link type="seealso" xref="manualeditionandcorrection"/>
    <desc>Opcións avanzadas para un mellor recoñecemento</desc>
</info>

<title>Posta a punto</title>

<p><app>OCRFeeder</app>  ten algunhas opcións avanzadas que se poden usar para efectuar un mellor recoñecemento. Estas opcións pódense escoller desde o cadro de diálogo <guiseq><gui>Edit</gui><gui>Preferencias</gui></guiseq>, baixo a lapela <gui>Recoñecemento</gui>.</p>

<p>A seguinte lista describe as opcións mencionadas:</p>
<list>
    <item><p><gui>Fixar quebras de liña e guionización</gui>: Os motores de OCR normalmente len o texto liña por liña e separan cada liña con unha quebra de liña. Ás veces, eso non é o que o usuario quere, porque o texto debe ser quebrado no medio dunha frase.</p>
    <p>Ao marcar esta opción fará que <app>OCRFeeder</app>  elimine caracteres de nova liña única despois de que o texto sexa recoñecido polos motores.</p>
    <p>Sólo eliminando liñas novas nun texto guionizado resultarían palabras indebidamente separadas, neste proceso tamén se detecta e se elimina a guionización.</p></item>
    <item><p><gui>Tamaño da xanela</gui>: O algoritmo de <app>OCRFeeder</app> para detectar os contidos dunha imaxe utiliza o concepto de <em>tamaño da xanela</em> que é a división da imaxe en xanelas pequenas. Unha xanela de tamaño menor significa que é máis probable detectar áreas de máis contido pero de tamaño moi pequeno que poden producir contidos que deben pertencer no seu lugar a unha área maior. Por outra banda, un tamaño de xanela maior significa menos divisións de contidos, pero pode acabar en contidos que deben ser subdivididos.</p>
    <p>Un bo tamaño de xanela debe ser lixeiramente maior que o espaciado de liñas de texto nunha imaxe.</p><p>Os usuarios poden querer axustar manualmente ese valor se o automático non produce ningunha área de contido válida, pero normalmente é máis fácil de usar o automático e facer as correccións necesarias directamente nas áreas de contido.</p></item>
    <item><p><gui>Mellorar a detección de columnas</gui>: Seleccione esta opción si <app>OCRFeeder</app> debe tentar dividir as áreas de contido detectadas horizontalmente (orixinando máis columnas). O valor que se usa para comprobar a existencia de espazos en branco nos contidos pode ser definido como automático ou manual cando as columnas non son detectadas correctamente.</p></item>
    <item><p><gui>Adjust content areas' bounds</gui>: The detected content
    areas sometimes have a considerable margin between their contents and
    the areas' edges. By checking this option, <app>OCRFeeder</app> will
    minimize those margins, adjusting the areas to its contents better.
    Optionally, a manual value can be check to indicate the maximum value
    of the adjusted margins.</p></item>
</list>

</page>
