<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="projects" xml:lang="uk">

<info>
    <link type="guide" xref="index#projects"/>
    <desc>Завантаження і збереження проектів</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Пилипчук Марія</mal:name>
      <mal:email>djka95 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Проекти</title>

<p>Іноді користувач може захотіти записати зроблену частину роботи, щоб продовжити її пізніше. На цей випадок <app>OCRFeeder</app> надає можливість зберігати та відкривати проекти.</p>

<p>Проекти — це стиснуті файли з розширенням <em>ocrf</em>, які містять інформацію про сторінки (зображення) і ділянки.</p>

<section id="save">
<title>Збереження проекту</title>

<p>Після початку роботи проект можна створити, клацнувши <guiseq><gui>Файл</gui><gui>Зберегти</gui></guiseq> або <guiseq><gui>Файл</gui><gui>Зберегти як…</gui></guiseq>. Також можна скористатись сполученнями клавіш <keyseq><key>Control</key><key>S</key></keyseq> або <keyseq><key>Control</key><key>Shift</key><key>S</key></keyseq>. З'явиться вікно збереження файла, в якому вказують назву проекту та його адресу.</p>

</section>

<section id="load">
<title>Завантаження проекту</title>

<p>Наявний проект можна завантажити клацнувши <guiseq><gui>Файл</gui><gui>Відкрити</gui></guiseq> або <keyseq><key>Ctrl</key><key>O</key></keyseq>.</p>

</section>

<section id="append">
<title>Додавання проекту</title>

<p>Іноді буває потрібно об'єднати два або більше проектів, щоб отримати єдиний документ зі сторінок, що належать різним проектам <app>OCRFeeder</app>. Цього досягають, додавши проект: при цьому сторінки довантажуються до поточного проекту. Щоб зробити це, клацають <guiseq><gui>Файл</gui><gui>Додати проект</gui></guiseq> і вибирають бажаний проект.</p>

</section>

<section id="clear">
<title>Очищення проекту</title>

<p>Якщо всю інформацію, наявну в проекті, потрібно вилучити (наприклад, щоб почати спочатку), це можна зробити вибравши <guiseq><gui>Зміни</gui><gui>Очистити проект</gui></guiseq>.</p>

</section>


</page>
