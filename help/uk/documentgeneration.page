<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="documentgeneration" xml:lang="uk">

<info>
    <link type="guide" xref="index#recognition"/>
    <link type="seealso" xref="automaticrecognition"/>
    <link type="seealso" xref="manualeditionandcorrection"/>
    <desc>Створення документа, придатного для редагування</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Пилипчук Марія</mal:name>
      <mal:email>djka95 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Породження документа</title>

<p>Зараз <app>OCRFeeder</app> породжує документи чотирьох форматів: <em>ODT</em><em>HTML</em>, <em>PDF</em> та <em>Звичайний текст</em>.</p>

<p>Після розпізнавання і, можливо, ручного редагування, можна створити документ, клацнувши <guiseq><gui>Файл</gui><gui>Експортувати…</gui></guiseq> та вибравши бажаний формат документа.</p>

<note style="tip"><p>Експортування в HTML породжує теку зі сторінками документа, поданими окремими файлами HTML. На кожній сторінці є посилання для переходу до попередньої і наступної сторінок. Ділянки, що містять зображення, зберігаються в підтеку з назвою <em>images</em>.</p></note>
<p>При експортуванні до <em>PDF</em>, можна вибрати один із двох типів <em>PDF</em>:</p>
<list>
<item>
<p>З нуля: якщо вибрано цей варіант, <app>OCRFeeder</app> створить новий <em>PDF</em> з нуля.</p>
</item>
<item><p>Придатний до пошуку PDF: якщо вибрано цей варіант, <app>OCRFeeder</app> створює <em>PDF</em> із початкових зображенням і невидимим текстом над ним, що уможливлює індексування вмісту т пошук тексту.</p>
</item>
</list>
<steps>
<title>Експортування розпізнаного документа</title>
<item><p>Після розпізнавання і, можливо, ручного редагування, можна створити документ, клацнувши <guiseq><gui>Файл</gui><gui>Експортувати…</gui></guiseq> (або натисніть <keyseq><key>SHIFT</key><key>CTRL</key><key>E</key></keyseq>) та вибравши бажаний формат документа.</p></item>
<item><p>Залежно від ваших потреб, виберіть, які сторінки має бути експортовано, усі сторінки (за допомогою пункту <gui>Усі</gui>) або лише поточну позначену (<gui>Поточна</gui>). Типовим варіантом є <gui>Усі</gui>. Після вибору варіанта натисніть кнопку <gui>Гаразд</gui>, щоб створити документ.</p></item>
<item><p>У останньому діалоговому вікні вкажіть назву файла результатів і натисніть кнопку <gui>Зберегти</gui>. Якщо файл із вибраною назвою існує, і ви хочете його перезаписати, натисніть кнопку <gui>Замінити</gui> у діалоговому вікні підтвердження дій.</p></item>
</steps>
</page>
