<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="documentgeneration" xml:lang="el">

<info>
    <link type="guide" xref="index#recognition"/>
    <link type="seealso" xref="automaticrecognition"/>
    <link type="seealso" xref="manualeditionandcorrection"/>
    <desc>Δημιουργία επεξεργάσιμου εγγράφου</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2013-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name> Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name> Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Δημιουργία εγγράφου</title>

<p>Το <app>OCRFeeder</app> προς το παρόν δημιουργεί τέσσερις μορφές εγγράφου: <em>ODT</em>, <em>HTML</em>, <em>PDF</em> και <em>απλό κείμενο</em>.</p>

<p>Μετά την αναγνώριση και την ενδεχόμενη χειροκίνητη επεξεργασία που έχει γίνει, είναι δυνατό να δημιουργηθεί ένα έγγραφο πατώντας <guiseq><gui>Αρχείο</gui><gui>Εξαγωγή…</gui></guiseq> και επιλέγοντας την επιθυμητή μορφή εγγράφου.</p>

<note style="tip"><p>Η εξαγωγή HTML δημιουργεί έναν φάκελο με τις αναπαριστάμενες σελίδες του εγγράφου από ένα αρχείο HTML. Σε κάθε σελίδα υπάρχουν σύνδεσμοι για μετάβαση στις προηγούμενες και επόμενες σελίδες. Οι περιοχές περιεχομένου εικόνας αποθηκεύονται σε έναν υποφάκελο που λέγεται <em>εικόνες</em>.</p></note>
<p>Κατά την εξαγωγή σε <em>PDF</em>, δύο τύποι <em>PDF</em> μπορούν να επιλεγούν:</p>
<list>
<item>
<p>Από την αρχή: Αν προτιμηθεί αυτή η επιλογή, το <app>OCRFeeder</app> δημιουργεί ένα νέο <em>PDF</em> από την αρχή.</p>
</item>
<item><p>Αναζητήσιμο PDF: Αν προτιμηθεί αυτή η επιλογή, το <app>OCRFeeder</app> δημιουργεί ένα <em>PDF</em> που εμφανίζει την αρχική εικόνα, αλλά με αόρατο κείμενο από πάνω του, κάνοντας δυνατή την δημιουργία ευρετηρίου ή αναζήτησης.</p>
</item>
</list>
<steps>
<title>Εξαγωγή του αναγνωρισμένου εγγράφου</title>
<item><p>Μετά την αναγνώριση και την ενδεχόμενη χειροκίνητη επεξεργασία που έχει γίνει, είναι δυνατό να δημιουργηθεί ένα έγγραφο πατώντας <guiseq><gui>Αρχείο</gui><gui>Εξαγωγή…</gui></guiseq>(ή πατώντας <keyseq><key>SHIFT</key><key>CTRL</key><key>E</key></keyseq>) και επιλέγοντας την επιθυμητή μορφή εγγράφου.</p></item>
<item><p>Ανάλογα με τις ανάγκες σας, επιλέξτε αν όλες οι σελίδες (με την επιλογή <gui>Όλες</gui>) ή μόνο η τρέχουσα επιλεγμένη (επιλογή <gui>Τρέχουσα</gui>) θα πρέπει να εξαχθούν. Η προεπιλεγμένη επιλογή είναι <gui>Όλες</gui>. Αφού κάνετε την επιλογή σας, πιέστε <gui>Εντάξει</gui> για να δημιουργηθεί το έγγραφο.</p></item>
<item><p>Στο τελευταίο παράθυρο διαλόγου, γράψτε το όνομα του αρχείου εξόδου και κάντε κλικ στο κουμπί <gui>Αποθήκευση</gui>. Εάν ένα αρχείο με αυτό το όνομα υπάρχει ήδη και θα θέλατε να το αντικαταστήσετε, κάντε κλικ στο κουμπί <gui>Αντικατάσταση</gui> του παραθύρου διαλόγου επιβεβαίωσης.</p></item>
</steps>
</page>
