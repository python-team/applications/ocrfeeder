<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="projects" xml:lang="el">

<info>
    <link type="guide" xref="index#projects"/>
    <desc>Φόρτωση και αποθήκευση έργων</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2013-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name> Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name> Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Έργα</title>

<p>Μερικές φορές κάποιος χρήστης μπορεί να θέλει να αποθήκευσει την πρόοδο της εργασίας που έγινε μέχρι τότε σε μια εικόνα και να συνεχίσει αργότερα. Για αυτήν την περίπτωση το <app>OCRFeeder</app> προσφέρει τη δυνατότητα αποθήκευσης και φόρτωσης των έργων.</p>

<p>Τα έργα είναι συμπιεσμένα αρχεία με την επέκταση <em>ocrf</em> που κρατά πληροφορίες για σελίδες (εικόνες) και περιοχές περιεχομένου.</p>

<section id="save">
<title>Αποθήκευση έργου</title>

<p>Έχοντας κάνει κάποια δουλειά σε μια εικόνα, ένα έργο μπορεί να δημιουργηθεί πατώντας <guiseq><gui>Αρχείο</gui><gui>Αποθήκευση</gui></guiseq> ή <guiseq><gui>Αρχείο</gui><gui>Αποθήκευση ως…</gui></guiseq>. Προαιρετικά, μπορούν να χρησιμοποιηθούν οι συντομεύσεις πληκτρολογίου <keyseq><key>Control</key><key>S</key></keyseq> ή <keyseq><key>Control</key><key>Shift</key><key>S</key></keyseq>. Ο διάλογος αποθήκευσης αρχείου θα εμφανιστεί τότε, έτσι ώστε το όνομα του έργου και η τοποθεσία να εισαχθούν.</p>

</section>

<section id="load">
<title>Φόρτωση έργου</title>

<p>Ένα υπάρχον έργο μπορεί να φορτωθεί πατώντας απλά <guiseq><gui>Αρχείο</gui><gui>Άνοιγμα</gui></guiseq> ή <keyseq><key>Control</key><key>O</key></keyseq>.</p>

</section>

<section id="append">
<title>Προσάρτηση έργου</title>

<p>Μερικές φορές είναι χρήσιμο να συγχωνευτούν δύο ή περισσότερα έργα για να δημιουργηθεί ένα μόνο έγγραφο με τις σελίδες αρκετών έργων <app>OCRFeeder</app>. Αυτό μπορεί να επιτευχθεί προσαρτώντας ένα έργο, που φορτώνει απλά τις σελίδες από ένα επιλεγμένο έργο στο τρέχον. Για να το κάνετε αυτό, πατήστε <guiseq><gui>Αρχείο</gui><gui>Προσάρτηση έργου</gui></guiseq> και επιλέξτε το επιθυμητό έργο.</p>

</section>

<section id="clear">
<title>Καθαρισμός έργου</title>

<p>Αν όλες οι πληροφορίες είναι ότι ένα έργο πρέπει να διαγραφτεί (για παράδειγμα, για να ξαναρχίσει), αυτό μπορεί να γίνει επιλέγοντας <guiseq><gui>Επεξεργασία</gui><gui>Καθαρισμός έργου</gui></guiseq>.</p>

</section>


</page>
