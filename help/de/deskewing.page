<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="deskewing" xml:lang="de">

<info>
    <link type="guide" xref="index#configuration"/>
    <link type="seealso" xref="manualeditionandcorrection"/>
    <desc>Korrektur von Schräglagen in einem Dokument</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2016-2017</mal:years>
    </mal:credit>
  </info>

<title>Schräglagenkorrektur</title>

<p>Einige Bilder, insbesondere jene die über einen Scanner hinzugefügt wurden, können gegenüber der eigentlichen Achse verdreht sein. Diese Schräglage erschwert die Erkennung des Bildes.</p>

<p><app>OCRFeeder</app> bietet einen Weg, die Schräglage eines Bildes automatisch zu korrigieren. Um ein geladenes Bild entsprechend zu bearbeiten, wählen Sie <guiseq><gui>Werkzeuge</gui><gui>Bilder begradigen</gui></guiseq>.</p>

<p>Dieser Vorgang kann auch so eingerichtet werden, dass diese Korrektur jedes Mal beim Hinzufügen eines Bildes automatisch ausgeführt wird. Wählen Sie hierzu <guiseq><gui>Bearbeiten</gui><gui>Einstellungen</gui></guiseq> und aktivieren Sie <gui>Bilder begradigen</gui> im Reiter <gui>Werkzeuge</gui>.</p>

<note style="warning"><p>Abhängig von der Größe und der Charakteristik des Bildes kann das Begradigen eines Bilder einige Zeit in Anspruch nehmen.</p></note>

</page>
