<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="addingimage" xml:lang="de">

<info>
    <link type="guide" xref="index#images"/>
    <desc>Hinzufügen eines zu erkennenden Bildes</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2016-2017</mal:years>
    </mal:credit>
  </info>

<title>Hinzufügen eines Bildes</title>

<p>Das Hinzufügen eines Bildes zu <app>OCRFeeder</app> ist üblicherweise der erste Schritt beim Umwandeln eines Dokuments.</p>

<p>Jedes hinzugefügte Bild stellt eine Seite im fertigen Dokument dar. Ein Vorschaubild wird im linken Bereich des <app>OCRFeeder</app>-Fensters angezeigt.</p>

<p>Die Reihenfolge der Seiten im sich ergebenden Dokument ist die gleiche wie die Reihenfolge der Bilder im Seitenbereich. Dadurch kann die Reihenfolge verändert werden, indem Sie die Vorschaubilder im Seitenbereich mit der Maus durch Ziehen und Ablegen neu anordnen.</p>

<p>Zum Hinzufügen eines Bildes wählen Sie <guiseq><gui>Datei</gui><gui>Bild hinzufügen</gui></guiseq>.</p>

<p>Um eine Seite zu löschen, wählen Sie <guiseq><gui>Bearbeiten</gui><gui>Seite löschen</gui></guiseq> oder klicken Sie mit der rechten Maustaste auf das Vorschaubild der Seite und wählen Sie <gui>Löschen</gui>.</p>

<section id="page-configuration">
<title>Seiteneinstellungen</title>

<p>Um die Seitengröße anzupassen, wählen Sie <guiseq><gui>Bearbeiten</gui><gui>Seite bearbeiten</gui></guiseq>. Wählen Sie anschließend eine Seitengröße aus, entweder indem Sie eine benutzerdefinierte Seitengröße angeben oder ein Standard-Papierformat in der Liste auswählen.</p>

</section>

</page>
