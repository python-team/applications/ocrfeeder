<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="projects" xml:lang="es">

<info>
    <link type="guide" xref="index#projects"/>
    <desc>Cargar y guardar proyectos</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  </info>

<title>Proyectos</title>

<p>En ocasiones, un usuario puede querer guardar el trabajo hecho hasta el momento en la imagen y continuarlo más tarde. Para estos casos, <app>OCRFeeder</app> ofrece la posibilidad de guardar y cargar proyectos.</p>

<p>Los proyectos son archivos comprimidos con la extensión <em>ocrf</em>, que almacena información osbre las páginas (imágenes) y las áreas de contenido.</p>

<section id="save">
<title>Guardar un proyecto</title>

<p>Después de trabajar un poco con la imagen, se puede crear un proyecto pulsando <guiseq><gui>Archivo</gui><gui>Guardar</gui></guiseq> o <guiseq><gui>Archivo</gui><gui>Guardar como…</gui></guiseq>. Opcionalmente, se pueden usar los atajos de teclado <keyseq><key>Ctrl</key><key>S</key></keyseq> o <keyseq><key>Ctrl</key><key>Mayús</key><key>S</key></keyseq>. Se mostrará un diálogo de guardado de archivos para que introduzca el nombre y la ubicación del proyecto.</p>

</section>

<section id="load">
<title>Cargar un proyecto</title>

<p>Se puede cargar un proyecto existente simplemente pulsando <guiseq><gui>Archivo</gui><gui>Abrir</gui></guiseq> o <keyseq><key>Ctrl</key><key>O</key></keyseq>.</p>

</section>

<section id="append">
<title>Añadir un proyecto</title>

<p>Algunas veces es útil combinar dos o más proyectos para crear un sólo documento con las páginas de varios proyectos de <app>OCRFeeder</app>. Esto se puede hacer añadiendo un proyecto, que simplemente carga las páginas de un proyecto elegido en el actual. Para hacer esto, pulse en <guiseq><gui>Archivo</gui><gui>Añadir proyecto</gui></guiseq> y elija el proyecto que quiere.</p>

</section>

<section id="clear">
<title>Limpiar un proyecto</title>

<p>Si se debe eliminar toda la información de un proyecto (por ejemplo, para empezar de nuevo), se puede hacer eligiendo <guiseq><gui>Editar</gui><gui>Limpiar proyecto</gui></guiseq>.</p>

</section>


</page>
